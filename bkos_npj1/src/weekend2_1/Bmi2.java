package weekend2_1;

import javax.swing.JOptionPane;

public class Bmi2 {
    
    public static void main(String[] args) {
        
        int waga1 = Integer.parseInt(JOptionPane.showInputDialog("ile ważysz?"));
        int wzrost1 = Integer.parseInt(JOptionPane.showInputDialog("ile masz wzrostu?"));
        
        double wzrostWMetrach1 = wzrost1 / 100.0;
        double bmi1 = waga1 / (wzrostWMetrach1 * wzrostWMetrach1);
        System.out.println("twoje BMI wynosi " + bmi1);
        
        System.out.println(String.format("mam %d wzrostu, ważę %d kg", 175, 90));
        
        // Jesli uwywamy doubla lib floata, wtedy piszemy:
        //System.out.println(String.format("mam %f wzrostu, ważę %f.2f kg", 173, 90.1234));
    }
}
