
package weekend2_1;

import javax.swing.JOptionPane;

public class Dzieci1 {
    
    public static void main(String[] args) {
        
        int cukierki1 = Integer.parseInt(JOptionPane.showInputDialog("Ile masz cukierków?"));
        int dzieci1 = Integer.parseInt(JOptionPane.showInputDialog("Ile masz dzieci?"));
        
        // Nawiasy przerabiaja zmienna w locie na doubla na potrzeby biezacego dziania i potem usuwaja
        // zmienna ta lotna, podczas gdy zmieniana zmienna <dzieci1> pozostaje integerem
        System.out.println(cukierki1 / (double) dzieci1); 
    }
}
