package weekend2_1;

import javax.swing.JOptionPane;

public class Nogi1 {
    
    public static void main(String[] args) {
        
        int nogi_kura1=2;
        int nogi_krowa1=4;
        int nogi_pajak1=8;
        
        int suma_nog1=0;
        
        int ilosc_kur1 = 0;
        int ilosc_krow1 = 0;
        int ilosc_pajakow1 = 0;
        
        ilosc_kur1 = Integer.parseInt(JOptionPane.showInputDialog("Podaj ilość posiadanych kur"));
        ilosc_krow1 = Integer.parseInt(JOptionPane.showInputDialog("Podaj ilość posiadanych krów"));
        ilosc_pajakow1 = Integer.parseInt(JOptionPane.showInputDialog("Podaj ilość pałętających się pająków"));
        
        if (ilosc_kur1 == 0) {
            System.out.println("Zakup więcej kur!");
        }
        if (ilosc_krow1 == 0) {
            System.out.println("Zaopatrz się w więcej krów!");
        }
        if (ilosc_pajakow1 == 0) {
            System.out.println("Przestań sprzątać, przyjdą pająki!");
        }
        
        suma_nog1 = (ilosc_kur1 * nogi_kura1) + (ilosc_krow1 + nogi_krowa1) + (ilosc_pajakow1 + nogi_pajak1);
        
        System.out.print("Liczba nóg w twoim inwentarzu to " + suma_nog1);
        if (suma_nog1 == 1) {
            System.out.println(" noga");
        } else {
            System.out.println(" nóg / nogi.");
        }
    }
}

