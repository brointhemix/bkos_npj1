package weekend2_1;

import javax.swing.JOptionPane;

public class ObjetoscDwudziestoscianu1 {
    
    public static void main(String[] args) {
         
        double dlugosc_krawedzi_20scianu1 = Double.parseDouble(JOptionPane.showInputDialog("Podaj długość krawędzi dwudziestościanu foremnego w centymetrach"));
        
        double objetosc_20scianu1 = (5.0 / 12.0) * Math.pow(dlugosc_krawedzi_20scianu1, 2) * (3.0 + Math.sqrt(5));
        
        // sout<TAB> = System.out.println()
        System.out.println("Objętość dwudziestościanu formnego wynosi " + objetosc_20scianu1 + " cm3");
    }
}

