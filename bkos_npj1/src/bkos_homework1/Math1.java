package bkos_homework1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Math1 {
    
    public static int addition1(int number1, int number2) {
        int result1;
        result1 = number1 + number2;

        return result1;
    }
    
    public static int subtraction1(int number1, int number2) {
        int result1;
        result1 = number1 - number2;

        return result1;
    }

    public static int multiplication1(int number1, int number2) {
        int result1;
        result1 = number1 * number2;

        return result1;
    }

    public static int division1(int number1, int number2) {
        int result1;
        result1 = number1 / number2;

        return result1;
    }
    
    //  
    public static Integer[] get1() {
        
        Integer number1 = null;
        Integer number2 = null;
        Integer number3[] = new Integer[2];        
        
        Scanner cli_input2 = new Scanner(System.in);
    
        while (number1 == null) {
            System.out.print("Provide first integer: ");
            try {
                number1 = cli_input2.nextInt();
            } catch (InputMismatchException ime1) {
                Gui1.print_error1();
                cli_input2.next();
            }
        }
        while (number2 == null) {
            System.out.print("Provide second integer: ");
            try {
                number2 = cli_input2.nextInt();
            } catch (InputMismatchException ime1) {
                Gui1.print_error1();
                cli_input2.next();
            }
        }
        
        number3[0] = number1;
        number3[1] = number2;
        
        return number3;
    }
}

// EOF
