package bkos_homework1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Integer number3[] = new Integer[2];
        Integer cumulative_result1 = null;
        String selector1 = "";

        Gui1.print_welcome1();
        Gui1.print_menu1();
        Gui1.print_endprogram1();
        
        Scanner cli_input1 = new Scanner(System.in);
        
        while (!selector1.contentEquals("x")) {
            
            System.out.print("Select your desired operation: ");
            while (!selector1.contentEquals("1") && !selector1.contentEquals("2") && !selector1.contentEquals("3") && !selector1.contentEquals("4") && !selector1.contentEquals("5") && !selector1.contentEquals("x")) {
                try {
                    selector1 = cli_input1.nextLine();
                } catch (InputMismatchException ime1) {
                    Gui1.print_error1();
                    cli_input1.next();
                }
            }
            
            if (selector1.contentEquals("5") && cumulative_result1 == null) {
                System.out.print("No previous calculation took place. Aborting...");
                selector1 = "";
                continue;
            }

            switch (selector1) {
                case "1":
                    number3 = Math1.get1();
                    cumulative_result1 = Math1.addition1(number3[0], number3[1]);
                    System.out.println(cumulative_result1);
                    selector1 = "";
                    break;
                case "2":
                    number3 = Math1.get1();
                    cumulative_result1 = Math1.subtraction1(number3[0], number3[1]);
                    System.out.println(cumulative_result1);
                    selector1 = "";
                    break;
                case "3":
                    number3 = Math1.get1();
                    cumulative_result1 = Math1.multiplication1(number3[0], number3[1]);
                    System.out.println(cumulative_result1);
                    selector1 = "";
                    break;
                case "4":
                    number3 = Math1.get1();
                    if (cumulative_result1 == null) {
                        if (number3[0] == 0 || number3[1] == 0) {
                            System.out.println("Division by 0 error. Aborting...");
                            selector1 = "";
                            continue;
                        }
                    } else if (number3[0] == 0 || number3[1] == 0 || cumulative_result1 == 0 ) {
                        System.out.println("Division by 0 error. Aborting...");
                        selector1 = "";
                        continue;
                    }
                    cumulative_result1 = Math1.division1(number3[0], number3[1]);
                    System.out.println(cumulative_result1);
                    selector1 = "";
                    break;
                default:
                    System.out.println("dupa");
                    break;
            }
        }
        
        if (selector1.contentEquals("x")) {
            System.out.println("Exit program.\n");
            System.exit(0);
        }
    }
}

// EOF
