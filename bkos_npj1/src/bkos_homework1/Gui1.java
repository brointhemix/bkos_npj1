package bkos_homework1;

public class Gui1 {
    
    public static void print_welcome1() {
        System.out.println(".:: bkos calculator1 ::.\n");
    }
    
    public static void print_menu1() {
        System.out.println("Pick your desired operation:");
        System.out.println("[1] Addition");
        System.out.println("[2] Subtraction");
        System.out.println("[3] Multiplication");
        System.out.println("[4] Division");
    }
    
    public static void print_menu2() {
        System.out.println("[5] Continue opetations on the previous calculation's result.");        
    }
    
    public static void print_endprogram1() {
        System.out.println("\n[x] Exit program\n");
    }
    
    public static void print_error1() {
        System.out.println("Must enter a number that is between -2147483648 and 2147483647 .");
    }
}
    
// EOF
