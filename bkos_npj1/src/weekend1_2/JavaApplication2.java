package weekend1_2;

public class JavaApplication2 {

    public static void main(String[] args) {
        
        System.out.println("JavaApplication2 " + args[0]);
        System.out.println("JavaApplication2 " + args[1]);
        System.out.println("JavaApplication2 " + args[2]);
        
        System.out.println("\n--------------\n");
        
        Integer args_i=args.length;
        System.out.println("JavaApplication2 " + args[--args_i]);
        System.out.println(args_i);
        
        
    }
    
}
