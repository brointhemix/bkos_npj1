package _pl.alx.weekend3_1.logika1;

public class Obliczenia1 {

    public static double policzBmi(double waga1, double wzrost1) {
        wzrost1 = pl.jkowalski.konwersje.konwerter.centymetryNaMetry((int) wzrost1);
        return waga1 / (wzrost1 * wzrost1);
    }
}
