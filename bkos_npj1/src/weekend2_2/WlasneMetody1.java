package weekend2_2;

public class WlasneMetody1 {

    private static void mojaMetoda() {
        System.out.println("Jestem twoją metodą.");
    }

    private static void zawolaj(String napis) {
        System.out.println(napis + "?");
        System.out.println(napis + "!");
    }

    private static int losuj() {
        return 4;
    }
    
    private static long zaokraglijWDol(double a) {
        long x = Math.round(a);
        if (a < x) {
            return --x;
        } else {
            return x;
        }
    }

    /*
    ===
     */
    public static void main(String[] args) {

        // Podanie klasy + nazwy metody
        WlasneMetody1.mojaMetoda();
        // Podanie tylko nazwy metody jeśli metoda w tej samej klasie
        mojaMetoda();

        /*
        ===
         */
        zawolaj("ciastka");
        
         /*
        ===
         */
         System.out.println(losuj());
    }
}

// EOF
