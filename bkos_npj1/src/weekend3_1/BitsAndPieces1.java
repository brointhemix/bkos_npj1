package weekend3_1;

import java.math.BigDecimal;
import javax.swing.JButton;
import javax.swing.JFrame;

public class BitsAndPieces1 {

    public static void main(String[] args) {
        //
//        String napis = "fiku miku";
//        napis = "konstantynopol to piękne miasto";
//        napis = napis.toUpperCase();
//        napis = "hopsasa".toUpperCase();
//        napis = "hop" + "sa".toUpperCase();
//        napis = ("hop" + "sa").toUpperCase();
//        napis = napis.replace("O", "iii");
//        System.out.println(napis);
        //
//        String a = "KOT";
//        String b = "kot".toUpperCase();
        // Not equal because both vars are referencing, so the reference is pointing to a diffret memory cell.
//        System.out.println(a == b);
//        System.out.println(a.equals(b));
        //
//        BigDecimal a1 = new BigDecimal("54.2");
//        BigDecimal a2 = new BigDecimal("54.3");
//        a1 = a1.subtract(a2);
//        System.out.println(a1);
        //
        // JFrame lyka tylko jeden guzik. JPanel lyka wiecej.
        JFrame okno = new JFrame();
        okno.setSize(800, 600);

        JButton guzik = new JButton("jakiś tekst");
        okno.add(guzik);

        okno.setVisible(true);
    }
}
