package weekend2_additionalday1;

import javax.swing.JOptionPane;

public class Witacz1 {
    
    public static void main(String[] args) {
        String imie1, imie2;
        
        imie1 = JOptionPane.showInputDialog("Podaj pierwsze imię.");
        imie2 = JOptionPane.showInputDialog("Podaj drugie imię.");
        
        System.out.println("Witajcie " + imie1 + " i " + imie2);
    }
}
