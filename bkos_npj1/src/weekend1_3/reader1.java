package weekend1_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class reader1 {
    public static Integer readFromInput(String s1, String s2) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Integer i1 = null;
        Integer i2 = null;
        try {
            s1 = br.readLine();
            i1 = Integer.parseInt(s1);
        } catch (IOException iox) {
            System.err.println("No data to read!!!" + "\n" + iox.toString());
        }

        try {
            s2 = br.readLine();
            i2 = Integer.parseInt(s2);
        } catch (IOException iox) {
            System.err.println("No data to read!!!" + "\n" + iox.toString());
        }
        
        return i2;
    }
}
